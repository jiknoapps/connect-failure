const { offline, timeout } = require('./session-store.js');
const pTimeout = require('p-timeout');

const get = async (url, options = {}) => {
	return await new Promise((resolve, reject) => {
		const headers = new Headers();
		headers.append(`content-type`, `application/json`);

		if (options.headers) {
			Object.keys(options.headers).forEach((header) => {
				headers.append(header, options.headers[header]);
			});
		}

		try {
			const response = fetch(url, {
				method: options.method || `GET`,
				body: options.body && JSON.stringify(options.body),
				headers,
				mode: 'cors',
			});

			pTimeout(
				response,
				timeout.get(),
				`Fetch request to ${url} timed out after trying for ${timeout.get()}ms.`
			)
				.then((res) => {
					if (res) {
						if (res.status < 200 || res.status >= 400) {
							offline.set(true);
							return reject(res);
						} else {
							offline.set(false);
							return resolve(res.json());
						}
					} else {
						offline.set(true);
						return reject(res);
					}
				})
				.catch((err) => {
					offline.set(true);
					return reject(err);
				});
		} catch (err) {
			offline.set(true);
			reject(err);
		}
	});
};

const post = async (url, body = {}) => await get(url, { method: `POST`, body });

const put = async (url, body = {}) => await get(url, { method: `PUT`, body });

module.exports = {
	get,
	post,
	put,
};
