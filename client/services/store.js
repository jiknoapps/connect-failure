const { writable } = require('./getable');

module.exports.lastVersion = writable({});
module.exports.dataOnClient = writable({});
module.exports.changes = writable([]);
module.exports.unsavedChanges = writable([]);
