const { writable, readable } = require('svelte/store');

module.exports.writable = (value) => {
	const { subscribe, set, update } = writable(value);

	const get = () => {
		let toReturn;
		subscribe((val) => (toReturn = val));
		return toReturn;
	};

	return {
		subscribe,
		set,
		update,
		get,
	};
};

module.exports.readable = (startVal, startFunc) => {
	const { subscribe } = readable(startVal, startFunc);

	const get = () => {
		let toReturn;
		subscribe((val) => (toReturn = val));
		return toReturn;
	};

	return {
		subscribe,
		get,
	};
};
