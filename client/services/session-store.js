const { writable } = require('./getable');

module.exports.saving = writable(false);
module.exports.offline = writable(false);

// Populated by ../init.js
module.exports.waitingInterval = writable(null);
module.exports.pollingInterval = writable(null);
module.exports.timeout = writable(null);
module.exports.url = writable(null);
module.exports.token = writable(null);
