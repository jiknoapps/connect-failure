const stores = require('./store');

const mirror = (store, storage) => {
	// Get the testData form localStorage
	const TEST_DATA = localStorage.getItem(storage);

	// If the testData exists, set it to the store.
	if (TEST_DATA) store.set(JSON.parse(TEST_DATA));

	// Everytime the store changes, update localStorage
	store.subscribe((val) =>
		localStorage.setItem(storage, JSON.stringify(val))
	);
};

const mirrorWithBranch = (store, storage) => {
	// Everytime the store changes, update localStorage
	store.subscribe((val) => {
		Object.keys(val).forEach((branch) => {
			localStorage.setItem(
				`${storage}-${branch}`,
				JSON.stringify(val[branch])
			);
		});
	});
};

module.exports = () => {
	// mirror every store besides dataOnClient to storage
	let storeArr = Object.keys(stores);

	storeArr.forEach((key) => {
		if (key != 'dataOnClient') mirror(stores[key], key);
	});
	mirrorWithBranch(stores.dataOnClient, 'dataOnClient');
};
