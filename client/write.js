const { changes, unsavedChanges } = require('./services/store');

module.exports = (path) => {
	const insert = (action, value, key) => {
		let pathArr = path.split('.');
		let branch = pathArr[0];
		pathArr.shift();
		let newTarget = pathArr.join('.');

		let newChange = {
			action,
			value,
			target: newTarget,
			key,
			branch,
		};
		unsavedChanges.update((data) => {
			data.push(newChange);
			return data;
		});
		changes.update((data) => {
			data.push({
				action,
				value,
				target: path,
				key,
			});
			return data;
		});
	};

	function createChild(key, value) {
		insert('create-child', value, key);
	}

	function createSibling(key, value) {
		insert('create-sibling', value, key);
	}

	function removeChild(key, value) {
		insert('remove-child', value, key);
	}

	function removeSibling(key, value) {
		insert('remove-sibling', value, key);
	}

	function editChild(key, value) {
		insert('edit-child', value, key);
	}

	function editSibling(key, value) {
		insert('edit-sibling', value, key);
	}

	function removeSelf(value) {
		insert('remove-self', value);
	}

	function editSelf(value) {
		insert('edit-self', value);
	}

	return {
		createChild,
		createSibling,
		removeChild,
		removeSibling,
		editChild,
		editSibling,
		removeSelf,
		editSelf,
	};
};
