const { dataOnClient, lastVersion } = require('./services/store');
const { readable } = require('./services/getable');

module.exports.load = (branch) => {
	lastVersion.update((val) => {
		if (!val[branch]) val[branch] = 0;
		return val;
	});
	dataOnClient.update((val) => {
		const TEST_DATA = localStorage.getItem(`dataOnClient-${branch}`);
		if (TEST_DATA) val[branch] = JSON.parse(TEST_DATA);
		else val[branch] = {};
		return val;
	});
};

module.exports.unload = (branch) => {
	dataOnClient.update((val) => {
		delete val[branch];
		return val;
	});
};

module.exports.loaded = readable([], (set) => {
	const unsubscribe = dataOnClient.subscribe((val) => {
		set(Object.keys(val));
	});
	return function stop() {
		unsubscribe();
	};
});
