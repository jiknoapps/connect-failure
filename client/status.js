const sessionStore = require('./services/session-store');
const { unsavedChanges } = require('./services/store');
const { readable } = require('./services/getable');

const saving = readable(false, function start(set) {
	let unsubscribe = sessionStore.saving.subscribe((val) => {
		set(val);
	});

	return function stop() {
		unsubscribe();
	};
});

const offline = readable(false, function start(set) {
	let unsubscribe = sessionStore.offline.subscribe((val) => {
		set(val);
	});

	return function stop() {
		unsubscribe();
	};
});

const changesSaved = readable(true, function start(set) {
	let unsubscribe = unsavedChanges.subscribe((changes) => {
		set(changes.length === 0);
	});

	return function stop() {
		unsubscribe();
	};
});

const summary = readable(true, function start(set) {
	let Usaving;
	let Uoffline;

	let UchangesSaved = changesSaved.subscribe(($changesSaved) => {
		Usaving = saving.subscribe(($saving) => {
			Uoffline = offline.subscribe(($offline) => {
				set(
					$changesSaved
						? 'saved'
						: $saving
						? 'saving'
						: $offline
						? 'offline'
						: 'working'
				);
			});
		});
	});

	return function stop() {
		UchangesSaved();
		Usaving();
		Uoffline();
	};
});

module.exports = {
	saving,
	offline,
	changesSaved,
	summary,
};
