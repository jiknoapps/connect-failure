const stores = require('./services/store');
const {
	saving,
	waitingInterval,
	token,
	url,
} = require('./services/session-store');
const { put } = require('./services/fetch');
const addChanges = require('../shared/add-changes');
const delay = require('delay');
const loadedStore = require('./load').loaded;

const dataOnClientStore = stores.dataOnClient;
const changesStore = stores.changes;
const unsavedChangesStore = stores.unsavedChanges;
const lastVersion = stores.lastVersion;

let dataOnClient;
dataOnClientStore.subscribe((data) => (dataOnClient = data));

let unsavedChanges;
unsavedChangesStore.subscribe((val) => (unsavedChanges = val));

let branchesLoaded;
loadedStore.subscribe((val) => (branchesLoaded = val));

const sendChanges = async () => {
	saving.set(true);

	await branchesLoaded.forEach(async (branch) => {
		let changesToSend = unsavedChanges.filter(
			(change) => change.branch === branch
		);
		let changesLeft = unsavedChanges.filter(
			(change) => change.branch !== branch
		);

		let response = await put(url.get(), {
			changes: changesToSend,
			authToken: token.get(),
			branch,
		}).catch((err) => {
			return err;
		});

		if (response.saved) {
			lastVersion.update((val) => {
				val[branch] = response.version;
				return val;
			});
			unsavedChangesStore.set(changesLeft);
		}
	});

	saving.set(false);
};

module.exports = () => {
	// If there are any changes, merge them into the data
	changesStore.subscribe((changes) => {
		if (changes.length > 0) {
			dataOnClientStore.set(addChanges(dataOnClient, changes));
			changesStore.set([]);
		}
	});

	// If there are any unsaved changes, try to save them
	let stopped = true;
	const interval = async () => {
		await sendChanges();
		await delay(waitingInterval.get());

		if (unsavedChanges.length != 0) interval();
		else stopped = true;
	};

	unsavedChangesStore.subscribe((changes) => {
		if (changes.length !== 0) {
			if (stopped) {
				stopped = false;
				interval();
			}
		}
	});
};

module.exports.sendChanges = sendChanges;
