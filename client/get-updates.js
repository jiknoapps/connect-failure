const { post } = require('./services/fetch');
const delay = require('delay');
const store = require('./services/store');
const addChanges = require('../shared/add-changes');
const { url, token } = require('./services/session-store');

const POLLING_INTERVAL = 1000;

let lastVersion;
store.lastVersion.subscribe((val) => (lastVersion = val));

let dataOnClient;
store.dataOnClient.subscribe((val) => (dataOnClient = val));

let pause;
store.unsavedChanges.subscribe((val) => (pause = val.length !== 0));

module.exports = async () => {
	if (!pause) {
		await Object.keys(dataOnClient).forEach(async (branch) => {
			let response = await post(url.get(), {
				lastVersion: lastVersion[branch],
				authToken: token.get(),
				branch,
			}).catch((err) => {
				console.error(err);
				return err;
			});

			if (response.summary) {
				if (response.changes) {
					store.dataOnClient.update((currentData) => {
						return addChanges(currentData, response.changes, true);
					});
				} else {
					store.dataOnClient.update((val) => {
						val[branch] = response.data;
						return val;
					});
				}
				store.lastVersion.update((val) => {
					val[branch] = response.version;
					return val;
				});
			}
		});
	}

	await delay(POLLING_INTERVAL);

	module.exports();
};
