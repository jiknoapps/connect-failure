const storeToStorage = require('./services/store-to-storage');
const getUpdates = require('./get-updates');
const sendUpdates = require('./send-updates');
const store = require('./services/session-store');

module.exports = (
	token,
	{
		dev = false,
		waitingInterval = 5000,
		timeout = 5000,
		pollingInterval = 1000,
	} = {}
) => {
	console.log('Starting connect-client...');

	const URL = dev ? 'http://localhost:4200' : 'https://apis.jikno.com';

	store.pollingInterval.set(pollingInterval);
	store.timeout.set(timeout);
	store.waitingInterval.set(waitingInterval);
	store.url.set(URL);
	store.token.set(token);

	// Mirror all stores to storage
	storeToStorage();
	sendUpdates();
	getUpdates();

	console.log('connect-client started!');
};
