const { url, token } = require('./services/session-store');

module.exports = (names, input) => {
	// Start with some vars
	const formData = new FormData();
	const filenames = [];
	const files = input.files;
	const length = files.length;

	// Loop through the files, pushing them into `formData` and recording the filenames
	for (let index = 0; index < length; index++) {
		const newName = `${names[index] ||
			Date.now() * Math.random()}-${Date.now() * Math.random()}`;
		filenames.push(newName);
		formData.append(`${token.get()}|${newName}`, files[index]);
	}

	// Create the options used for formData requests
	const options = {
		method: 'POST',
		body: formData,
		headers: {},
	};

	// Create the basic request
	const post = async () => await fetch(`${url.get()}/upload-media`, options);

	// Wrap it in a promise
	const promise = async () => {
		return await post()
			.then((res) => res.json())
			.then((obj) => {
				obj.filenames = filenames;
				return obj;
			});
	};

	// Finish
	return {
		post,
		promise,
	};
};
