const { readable } = require('./services/getable');
const makeTree = require('../shared/make-tree');
const { dataOnClient } = require('./services/store');

module.exports = (path) => {
	console.log(`Reading '${path}'...`);

	return readable({}, function start(set) {
		let unsubscribe = dataOnClient.subscribe((data) => {
			let pathArr = [];
			const copy = data;

			if (path && path !== '' && path !== '.') pathArr = path.split('.');
			set(makeTree(pathArr, copy));
		});

		return function stop() {
			unsubscribe();
		};
	});
};
