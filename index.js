const start = require('./client/init');
const status = require('./client/status');
const read = require('./client/read');
const write = require('./client/write');
const { load, unload, loaded } = require('./client/load');
const uploadMedia = require('./client/upload-media');

module.exports = {
	start,
	status,
	read,
	write,
	load,
	unload,
	loaded,
	uploadMedia,
};
