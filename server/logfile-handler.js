const { read, write } = require('./database');
const getPath = require('./services/get-path');

const defaultConfiguration = {
	request: 0,
	requestChangesSent: 0,
	handledChanges: 0,
	uploads: 0,
	errors: {},
	warnings: {},
};

module.exports = async (
	token,
	{
		request,
		requestChangesSent,
		handledChanges,
		upload,
		newErrorMessage,
		newErrorCode,
		newWarningMessage,
		newWarningCode,
	}
) => {
	if (
		!token &&
		!request &&
		!requestChangesSent &&
		!handledChanges &&
		!upload &&
		!newErrorMessage &&
		!newWarningMessage
	)
		return;
	const path = await getPath(token);

	// Get the current log
	let currentLog = await read(path, 'project.log');
	if (JSON.stringify(currentLog) == `{}`) currentLog = defaultConfiguration;
	else console.log('used');

	// perform the right action
	if (request) currentLog.request++;
	if (requestChangesSent) currentLog.requestChangesSent++;
	if (handledChanges) currentLog.handledChanges++;
	if (upload) currentLog.uploads++;
	const milliseconds = new Date().getTime();
	// Errors and warnings are expected to have three values: `message`, `code`, and `time`.
	if (newErrorMessage)
		currentLog.errors[milliseconds] = {
			message: newErrorMessage,
			code: newErrorCode || '',
			date: milliseconds,
		};
	if (newWarningMessage)
		currentLog.errors[milliseconds] = {
			message: newWarningMessage,
			code: newWarningCode || '',
			date: milliseconds,
		};

	// write it to the log
	await write(path, 'project.log', currentLog);
};

module.exports.defaultConfiguration = defaultConfiguration;
