/*
	* This connects the is the file that makes it easy to access the database.
	* To read data from the database:

			const posts = await read('posts');
			// returns an object.

		* `read()` assumes that the format of the file is json, but if it is not, just specify a second param:

			const html = await read('index', 'html');
			// returns a string

*/

const fs = require('fs');
const writeFileWithDirPower = require('write');

const write = async (path, file, data) => {
	return new Promise((resolve, reject) => {
		writeFileWithDirPower(
			`${path}/${file}.json`,
			JSON.stringify(data),
			(err) => {
				if (err) reject(err);
				else resolve();
			}
		);
	});
};

const read = async (path, file) => {
	return new Promise((resolve, reject) => {
		fs.readFile(`${path}/${file}.json`, 'utf-8', async (err, data) => {
			if (err) {
				if (err.code == 'ENOENT') {
					const newFileContent =
						file.split('.').length < 2
							? { version: 0, data: {} }
							: {};
					try {
						await write(path, file, newFileContent);
						resolve(newFileContent);
					} catch (e) {
						reject(e);
					}
				} else {
					reject(err);
				}
			} else resolve(JSON.parse(data));
		});
	});
};

module.exports = {
	read,
	write,
};
