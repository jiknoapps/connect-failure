const { read } = require('../database');
const path = `../database`;

const getProjectId = async (token) => {
	let folder = await read(`${path}`, 'database-users');
	if (folder[token] === undefined) {
		return Promise.reject({
			resStatus: 403,
			message: 'Invalid API key',
			code: 'EACCESS',
		});
	}
	return folder[token];
};

module.exports = async (token) => {
	let id = await getProjectId(token);
	return `${path}/${id}`;
};

module.exports.pathToDatabase = path;

module.exports.projectID = getProjectId;
