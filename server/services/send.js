// Send data
function sendError(err, res) {
	res.status(err.resStatus || 500).send({
		message: err.message,
		code: err.code,
	});
}

function sendOk(data, res) {
	res.status(200).send(data);
}

module.exports.sendOk = sendOk;
module.exports.sendError = sendError;
