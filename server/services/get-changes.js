const { read } = require('../database');
const getPath = require('./get-path');

module.exports = async (token, version) => {
	const PATH = await getPath(token);
	const CHANGES = await read(PATH, 'data.changes');

	let returnArr = [];

	Object.keys(CHANGES).forEach((v) => {
		if (Number(v) > version) {
			returnArr = returnArr.concat(CHANGES[v]);
		}
	});

	return returnArr;
};
