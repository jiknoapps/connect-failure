const express = require('express');
const app = express();
const { read, write } = require('./database');
const getPath = require('./services/get-path');
const getChanges = require('./services/get-changes');
const addChanges = require('../shared/add-changes');
const logfileHandler = require('./logfile-handler');
const multer = require('multer');
const path = require('path');
const mkdirp = require('mkdirp');
const pathToDatabase = getPath.pathToDatabase;
const projectId = getPath.projectID;
const serveIndex = require('serve-index');
const { sendError, sendOk } = require('./services/send');

// CORS
app.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader(
		'Access-Control-Allow-Methods',
		'GET, POST, OPTIONS, PUT, PATCH, DELETE'
	);
	res.setHeader(
		'Access-Control-Allow-Headers',
		'X-Requested-With,content-type'
	);
	res.setHeader('Access-Control-Allow-Credentials', true);
	next();
});

// JSON
app.use(express.json());

// Serve the `uploads` folder into `/public`
let pathToUploads = `${pathToDatabase}/uploads`;
app.use(
	'/public',
	express.static(pathToUploads),
	serveIndex(pathToUploads, {
		icons: true,
		template: './server/pages/404.html',
	})
);

// Serve /favicon.ico
app.get('/favicon.ico', (req, res) => {
	res.status(200).sendFile(`${__dirname}/pages/favicon.ico`);
});

app.post('/', async (req, res) => {
	try {
		const { lastVersion, authToken, branch } = req.body;
		const PATH = await getPath(authToken);
		const { version, data } = await read(PATH, branch);

		let anticipatedResponse = {
			summary: false,
		};

		if (version != lastVersion) {
			if (lastVersion + 20 >= version && lastVersion <= version) {
				anticipatedResponse = {
					summary: true,
					version,
					changes: await getChanges(authToken, lastVersion),
				};
			} else {
				anticipatedResponse = {
					summary: true,
					version,
					data,
				};
			}
			logfileHandler(authToken, { requestChangesSent: true });
		}
		logfileHandler(authToken, { request: true });

		sendOk(anticipatedResponse, res);
	} catch (err) {
		sendError(err, res);
	}
});

app.put('/', async (req, res) => {
	try {
		const { authToken, changes, branch } = req.body;
		const PATH = await getPath(authToken);
		const { version, data } = await read(PATH, branch);
		const newVersion = version + 1;

		const changesLog = await read(PATH, `${branch}.changes`);

		// Add the new changes to the log
		changesLog[newVersion] = changes;
		// Any changes over 20 versions old are deleted
		Object.keys(changesLog).forEach((v) => {
			if (v < newVersion - 20 || v > newVersion) delete changesLog[v];
		});

		// Write to the changes into the log
		await write(PATH, `${branch}.changes`, changesLog);
		// Add the changes into the file
		await write(PATH, branch, {
			version: newVersion,
			data: addChanges(data, changes),
		});

		logfileHandler(authToken, { handledChanges: true });

		sendOk({ saved: true, version: newVersion }, res);
	} catch (err) {
		sendError(err, res);
	}
});

// Allow getting the `project.log` file.
app.post('/logfile', async (req, res) => {
	try {
		const { authToken } = req.body;
		const PATH = await getPath(authToken);
		const logfile = await read(PATH, 'project.log');
		logfileHandler(authToken, { request: true });

		sendOk(logfile, res);
	} catch (err) {
		sendError(err, res);
	}
});

// Allow uploading media
// Connect expects file.fieldname to have this format, `<api-token>|<unique-name>`;
const storage = multer.diskStorage({
	destination: async (req, file, cb) => {
		// Get the path
		const token = file.fieldname.split('|')[0];
		const id = await projectId(token);
		const PATH = `${pathToDatabase}/uploads/${id}`;

		// Create the directory
		mkdirp(PATH, () => {
			// Return the path
			cb(null, PATH);
		});

		// Write the request to the logfile
		logfileHandler(token, { upload: true });
	},
	filename: (req, file, cb) => {
		// Return the filename
		cb(
			null,
			`${file.fieldname.split('|')[1]}${path.extname(file.originalname)}`
		);
	},
});

const upload = multer({ storage }).any();

app.post(`/upload-media`, async (req, res) => {
	// Upload the file
	upload(req, res, (err) => {
		// Catch and send the error
		if (err) res.status(500).send({ uploaded: false, err });
		// Done
		else res.status(200).send({ uploaded: true });
	});
});

// Respond to 404s
app.use((req, res) => {
	res.status(404).sendFile(`${__dirname}/pages/404.html`);
});

app.listen(4200, () => console.log(`Listening on port 4200...`));
