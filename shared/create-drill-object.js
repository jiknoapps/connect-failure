const fs = require('fs');

let innerData = `
if (keys.length == 0) obj = value;
`;

for (let length = 1; length <= 50; length++) {
	let dig = `obj`;
	for (let key = 0; key < length; key++) {
		dig += `[keys[${key}]]`;
	}
	dig += ` = value`;

	innerData += `
	else if (keys.length == ${length}) ${dig};
	`;
}

let data = `
module.exports = function drillObject(keys, obj, value) {
	${innerData}
	else throw new Error('Max object depth is 50');

	return obj;
}
`
	.replace(/\n/g, '')
	.replace(/	/g, '');

const wrappedData = `"use strict";
${data}`;

fs.writeFile('./shared/drill-object.js', wrappedData, 'utf-8', (err) => {
	if (err) console.error(err);
	else console.log('Done!');
});
