module.exports = (keysArray, obj) => {
	const arr = keysArray.reduce((parent, key) => {
		try {
			let child = parent[key];
			return child;
		} catch (e) {
			// do nothing
		}
	}, obj || {});

	return arr || {};
};
