'use strict';

const drillObject = require('./drill-object');
const makeTree = require('./make-tree');

module.exports = (source, changes = [], applyBranch = false) => {
	changes.forEach((change) => {
		const { action } = change;
		if (applyBranch) {
			if (isTarget(change.target)) {
				change.target = `${change.branch}.${change.target}`;
			} else change.target = change.branch;
		}

		if (action == 'create-child') source = createChild(source, change);
		else if (action === 'create-sibling')
			source = createSibling(source, change);
		else if (action === 'remove-child')
			source = removeChild(source, change);
		else if (action === 'remove-sibling')
			source = removeSibling(source, change);
		else if (action === 'edit-child') source = editChild(source, change);
		else if (action === 'edit-sibling')
			source = editSibling(source, change);
		else if (action === 'remove-self') source = removeSelf(source, change);
		else if (action === 'edit-self') source = editSelf(source, change);
		else throw new Error('No valid action specified.');
	});

	return source;
};

function createChild(source, change) {
	const target = getTarget(change);
	const { key, value } = change;

	return drillObject(target.concat([key]), source, value);
}

function createSibling(source, change) {
	return createChild(source, popTarget(change));
}

function removeChild(source, change) {
	const target = getTarget(change);

	let children = makeTree(target, source);
	delete children[change.key];

	return drillObject(target, source, children);
}

function removeSibling(source, change) {
	return removeChild(source, popTarget(change));
}

function editChild(source, change) {
	source = removeChild(source, change);
	change.key = change.newKey;
	return createChild(source, change);
}

function editSibling(source, change) {
	return editChild(source, popTarget(change));
}

function removeSelf(source, change) {
	return removeChild(source, popTarget(change, true));
}

function editSelf(source, change) {
	return editChild(source, popTarget(change, true));
}

function isTarget(target) {
	return (
		target != '.' && target != '' && target != null && target != undefined
	);
}

function getTarget(change) {
	if (isTarget(change.target)) return change.target.split('.');
	else return [];
}

function popTarget(change, addPoppedToKey = false) {
	change.target = change.target.split('.');
	if (addPoppedToKey) change.key = change.target[change.target.length - 1];
	change.target.pop();
	change.target = change.target.join('.');
	return change;
}
