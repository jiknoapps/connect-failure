const { test } = require('tape-modern');
const addChanges = require('../shared/add-changes');
const isSame = require('is-deep-eq');

module.exports = () => {
	test(`Test "create-child"`, (t) => {
		t.ok(
			isSame(
				addChanges(
					{
						example: true,
						lists: { list1: { tasks: { task1: 'feed the dog' } } },
					},
					[
						{
							action: 'create-child',
							target: 'lists.list1.tasks',
							value: 'walk the monkey',
							key: 'task2',
						},
					]
				),
				{
					example: true,
					lists: {
						list1: {
							tasks: {
								task1: 'feed the dog',
								task2: 'walk the monkey',
							},
						},
					},
				}
			)
		);
	});

	test(`Test "create-sibling"`, (t) => {
		t.ok(
			isSame(
				addChanges(
					{
						example: true,
						lists: { list1: { tasks: { task1: 'feed the dog' } } },
					},
					[
						{
							action: 'create-sibling',
							target: 'lists.list1.tasks.tasks1',
							value: 'walk the monkey',
							key: 'task2',
						},
					]
				),
				{
					example: true,
					lists: {
						list1: {
							tasks: {
								task1: 'feed the dog',
								task2: 'walk the monkey',
							},
						},
					},
				}
			)
		);
	});

	test(`Test "remove-child"`, (t) => {
		t.ok(
			isSame(
				addChanges(
					{
						example: true,
						lists: {
							list1: {
								tasks: {
									task1: 'feed the dog',
									task2: 'walk the monkey',
								},
							},
						},
					},
					[
						{
							action: 'remove-child',
							target: 'lists.list1.tasks',
							key: 'task2',
						},
					]
				),
				{
					example: true,
					lists: { list1: { tasks: { task1: 'feed the dog' } } },
				}
			)
		);
	});

	test(`Test "remove-sibling"`, (t) => {
		t.ok(
			isSame(
				addChanges(
					{
						example: true,
						lists: {
							list1: {
								tasks: {
									task1: 'feed the dog',
									task2: 'walk the monkey',
								},
							},
						},
					},
					[
						{
							action: 'remove-sibling',
							target: 'lists.list1.tasks.task1',
							key: 'task2',
						},
					]
				),
				{
					example: true,
					lists: { list1: { tasks: { task1: 'feed the dog' } } },
				}
			)
		);
	});

	test(`Test "edit-child"`, (t) => {
		t.ok(
			isSame(
				addChanges(
					{
						example: true,
						lists: {
							list1: {
								tasks: {
									task1: 'feed the dog',
									task2: 'walk the monkey',
								},
							},
						},
					},
					[
						{
							action: 'edit-child',
							target: 'lists.list1.tasks',
							key: 'task2',
							newKey: 'task10',
							value: 'eat pizza',
						},
					]
				),
				{
					example: true,
					lists: {
						list1: {
							tasks: {
								task1: 'feed the dog',
								task10: 'eat pizza',
							},
						},
					},
				}
			)
		);
	});

	test(`Test "edit-sibling"`, (t) => {
		t.ok(
			isSame(
				addChanges(
					{
						example: true,
						lists: {
							list1: {
								tasks: {
									task1: 'feed the dog',
									task2: 'walk the monkey',
								},
							},
						},
					},
					[
						{
							action: 'edit-sibling',
							target: 'lists.list1.tasks.task1',
							key: 'task2',
							newKey: 'task10',
							value: 'eat pizza',
						},
					]
				),
				{
					example: true,
					lists: {
						list1: {
							tasks: {
								task1: 'feed the dog',
								task10: 'eat pizza',
							},
						},
					},
				}
			)
		);
	});

	test(`Test "remove-self"`, (t) => {
		t.ok(
			isSame(
				addChanges(
					{
						example: true,
						lists: {
							list1: {
								tasks: {
									task1: 'feed the dog',
									task2: 'walk the monkey',
								},
							},
						},
					},
					[
						{
							action: 'remove-self',
							target: 'lists.list1.tasks.task1',
						},
					]
				),
				{
					example: true,
					lists: { list1: { tasks: { task2: 'walk the monkey' } } },
				}
			)
		);
	});

	test(`Test "edit-self"`, (t) => {
		t.ok(
			isSame(
				addChanges(
					{
						example: true,
						lists: {
							list1: {
								tasks: {
									task1: 'feed the dog',
									task2: 'walk the monkey',
								},
							},
						},
					},
					[
						{
							action: 'edit-self',
							target: 'lists.list1.tasks.task2',
							newKey: 'task10',
							value: 'eat pizza',
						},
					]
				),
				{
					example: true,
					lists: {
						list1: {
							tasks: {
								task1: 'feed the dog',
								task10: 'eat pizza',
							},
						},
					},
				}
			)
		);
	});
};
